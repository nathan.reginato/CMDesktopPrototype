(function() {

    // Example using Ramda and Camen
    // http://camanjs.com
    // https://ramdajs.com

    const fileInput = document.getElementById("the-file-input");
    
    const containerElement = document.getElementById("some_container_div");


    
    const createImage = function(url) {
        const img = document.createElement("img");
        img.src = url;
        return img;
    }

    const attachElement = R.curry((parent, element) => parent.appendChild(element));

    const attachImageToContainer = R.tap(attachElement(containerElement));
    
    const getTargetResult = R.compose(R.prop('result'), R.prop('target'));

    const createPhotoInstance = R.compose(createImage, getTargetResult);
    
    const attachPhoto = R.compose(attachImageToContainer, createPhotoInstance);

    const readFiles = function(file) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = attachPhoto;
    };

    const getFiles = R.values;

    const getAndRead = R.compose(R.map(readFiles), getFiles);
    
    fileInput.onchange = function() {
        getAndRead(this.files)
    }
})();